ARG ALPINE_TAG=latest
ARG DEBIAN_TAG=latest

FROM alpine:3 AS cert
# hadolint ignore=DL3018
RUN apk add --no-cache ca-certificates

FROM scratch as core
COPY rootfs/common /
COPY --from=cert /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

FROM alpine:${ALPINE_TAG} as alpine
COPY --from=core / /
COPY rootfs/alpine /
RUN echo '@edge/main http://dl-cdn.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories
RUN echo '@edge/community http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories
RUN echo '@edge/testing http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
RUN setup

FROM debian:${DEBIAN_TAG} as debian
COPY --from=core / /
RUN setup

FROM docker:19 as test
RUN apk add --no-cache bats~=1 docker-compose~=1
WORKDIR /app
COPY docker-compose.yml .
COPY tests tests
COPY tests.bats .
CMD ["bats", "."]
