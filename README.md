# Core

Nexylan core docker image.

Contains only:

- Common configuration scripts
- Common resource files
- Standalone working binaries

## Usage

### Dockerfile

#### Alpine

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/core:x.y.z-alpine
RUN ...
```

#### Installing edge packages

You have two ways to install packages from the [edge branch of Alpine](https://pkgs.alpinelinux.org/packages?name=&branch=edge):

1. Use the `alpine-edge` Docker tag: `docker run registry.gitlab.com/nexylan/docker/core:x.y.z-alpine-edge apk add lab`
2. Use the edge repositories: `docker run registry.gitlab.com/nexylan/docker/core:x.y.z-alpine apk add lab@edge/community`

#### Debian

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/core:x.y.z-debian
RUN ...
```

#### Core

This may be used if you are only interested by the Nexylan's core files.

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/core:x.y.z as core
FROM whatever
COPY --from=core / /
RUN setup
RUN ...
```

⚠ ️Do not copy the rootfs of alpine or debian variant.
Just copy from `core` base image and run the `setup` command.

### CLI usage

You need to use an image variant to make it directly working:

```bash
docker run registry.gitlab.com/nexylan/docker/core:x.y.z-alpine bash
```

## Tools

The core image provides a bunch of pre-installed tools to simplify your usage.

### CA certificates

The core images has embedded CA certificates crt file. You can extract it directly like this:

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/core:x.y.z as core
FROM whatever
COPY --from=core /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
```

They are installed from the [Alpine package][alpine_pkg_ca-certificates]

### Bin install

Install binaries thanks to a single command line:

```bash
bin-install kompose https://github.com/kubernetes/kompose/releases/download/v1.18.0/kompose-linux-amd64
```

### HTTPie

🌐 https://httpie.org/

On Steroids curl like tool.

### Wait for it

🌐 https://github.com/vishnubob/wait-for-it

Useful to avoid container crash du to not ready services.

### GitLab Merge Request

The `mr_prepare` and `mr_submit` scripts are provided to facilitate the merge request automation.

You will generally use them on a `.gitlab-ci.yml` script:

```yaml
update:
  stage: maintain
  image: registry.gitlab.com/nexylan/docker/core:x.y.z-alpine
  variables:
    NEXY_MR_BRANCH_NAME: bot/update
  script:
    # Necessary binary to make the script working properly.
    - apk add git~=2 lab@edge/community~=0.17
    - mr_prepare
    - NEW_VERSION="what-you-want"
    - your-update-stuff
    - 'mr_submit "fix: what you just updated to ${NEW_VERSION}"'
  only:
    - master
```

## Development

```
make
```

[alpine_pkg_ca-certificates]: https://pkgs.alpinelinux.org/package/edge/main/s390x/ca-certificates "Alpine package"
