#!/usr/bin/env bats

function run() {
  docker-compose run alpine $@
  docker-compose run debian $@
}

@test "ls" {
  run ls
}

@test "httpie" {
  run http --version
}

@test "wait-for-it" {
  run which wait-for-it
}

@test "wait-for-it test" {
  run wait-for-it google.com:80 --timeout=60
}

@test "gnu grep" {
  # BusyBox grep does not have the `--version` flag.
  run grep --version
}

@test "node build" {
  docker build \
    --tag "${IMAGE}-build-test" \
    --build-arg "IMAGE=${IMAGE}" \
    tests/node-build
}

@test "go-resolver with /etc/hosts" {
  docker build \
    --tag "${IMAGE}-go-resolver" \
    --build-arg "IMAGE=${IMAGE}-alpine" \
    tests/go-resolver
  docker run --rm --add-host=google.com:1.2.3.4 ${IMAGE}-go-resolver
}
