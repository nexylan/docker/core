package main

import (
	"log"
	"net"
)

func main() {
	ips, err := net.LookupIP("google.com")
	if err != nil {
		log.Fatalf("%v", err)
	}
	if ips[0].String() != "1.2.3.4" {
		log.Fatalf("Not working /etc/host resolving. Got %s. Expected 1.2.3.4", ips[0])
	}
}
